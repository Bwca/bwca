# Hello

## Well, what am I?

I am a frontend developer, who mostly uses typescript as you see from the stats below. Sometimes I write [articles](https://dev.to/bwca) to share my findings ^_^

<a href="https://github.com/bwca/github-readme-stats">
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=bwca&theme=synthwave&count_private=true" />
</a>

Coding is my hobby which became my work. So I need another hobby...

### Well, what I maintain?

#### NPM packages
* [iso-datestring-validator](https://www.npmjs.com/package/iso-datestring-validator) for validating string representation of dates;
* [property-string-path](https://www.npmjs.com/package/property-string-path) for generating string representations of all property paths in an object of any depth.

And a bunch of less popular ones.

#### Chrome extensions
* [Promoted posts blocker](https://github.com/Bwca/browser-extension__linkedin-antiprom) for removing promoted posts from LinkedinIn.
* [Jira executor filters](https://github.com/Bwca/browser-extension__jira-executors-filter) for adding executor icons to jira board.
* [Magic 8 Ball](https://chrome.google.com/webstore/detail/magic-8-ball/hkhipapgpdambeamafdciafnlgppedlg?hl=en) available in Chrome Store.

#### Games? Kinda
* [Rigged slot machine](https://github.com/Bwca/test__slot-machine), it was actually a test task, but I still play it sometimes, the thing is addictive. [Deployed here, check it out](https://bwca.github.io/test__slot-machine/) :D
* [Magic 8 ball](https://github.com/Bwca/app__magic-8-ball), it knows all the answers and now comes in 3d. Can be installed as an app, go ask your questions [here](https://bwca.github.io/app__magic-8-ball/) :)

